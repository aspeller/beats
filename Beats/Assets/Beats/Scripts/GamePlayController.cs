﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Beats
{
    public class GamePlayController : MonoBehaviour
    {
        [Header("Inputs")]
        [SerializeField] KeyCode _left;
        [SerializeField] KeyCode _down;
        [SerializeField] KeyCode _up;
        [SerializeField] KeyCode _right;

        [Header("Track")]
        [SerializeField]
        private Track _track;
        /// <summary>
        /// The current track.
        /// </summary>
        public Track track { get { return _track; } }

        public float secondsPerBeat { get; private set; }
        public float BeatsPerSecond { get; private set; }

        private bool _completed;
        private bool _played;

        TrackView _trackView;

        private WaitForSeconds waitAndStop;

        //Singleton Design Pattern
        private static GamePlayController _instance;
        public static GamePlayController Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = (GamePlayController)GameObject.FindObjectOfType(typeof(GamePlayController));
                }
                return _instance;
            }
        }

        #region MonoBehaviour Methods
        private void OnDestroy()
        {
            _instance = null;
        }

        private void Awake()
        {
            _instance = this;
            secondsPerBeat = track.bpm / 60f;
            BeatsPerSecond = 60f / track.bpm;
            waitAndStop = new WaitForSeconds(BeatsPerSecond * 2);

            // Bind the TrackView to the Controller
            _trackView = FindObjectOfType<TrackView>();
            if (!_trackView)
            {
                Debug.LogWarning("No TrackView found in the current scene");
            }
        }

        // Use this for initialization
        void Start()
        {
            InvokeRepeating("NextBeat",0f, BeatsPerSecond);
        }

        // Update is called once per frame
        void Update()
        {
            // TODO: Move outside of the GamePlayController

            if (_played || _completed)
            {
                return;
            }

            if (Input.GetKeyDown(_left))
            {
                PlayBeat(0);
            }
            if (Input.GetKeyDown(_down))
            {
                PlayBeat(1);
            }
            if (Input.GetKeyDown(_up))
            {
                PlayBeat(2);
            }
            if (Input.GetKeyDown(_right))
            {
                PlayBeat(3);
            }
        }
        #endregion

        #region GamePlay

        private int _current;
        public int current
        {
            get { return _current; }
            set
            {
                if (value != _current)
                {
                    _current = value;

                    if (_current == track.beats.Count)
                    {
                        CancelInvoke("NextBeat");
                        _completed = true;
                        StartCoroutine(WaitAndStop());
                    }
                    
                }
            }
        }

        void PlayBeat(int input)
        {
            _played = true;

            if (_track.beats[current] == -1)
            {
                //Debug.Log(string.Format("{0} played untimely", input));
            }
            else if (_track.beats[current] == input)
            {
                //Debug.Log(string.Format("{0} played right", input));
                _trackView.TriggerBeatView(current, TrackView.Trigger.Right);
            } else
            {
                //Debug.Log(string.Format("{0} played, {1} expected", input, _track.beats[current]));
                _trackView.TriggerBeatView(current, TrackView.Trigger.Wrong);
            }
        }

        void NextBeat()
        {
            //Debug.Log("Tick");

            if (!_played && _track.beats[current] != -1)
            {
                Debug.Log(string.Format("{0} missed", _track.beats[current]));
                _trackView.TriggerBeatView(current, TrackView.Trigger.Missed);
            }
            _played = false;
            current++;
        }

        private IEnumerator WaitAndStop()
        {
            yield return waitAndStop;
            enabled = false;
        }
        #endregion
    }
}